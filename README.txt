
Using spearmint wrapper: example
---------------------------

import spearmint_wrapper

Task: the robo task to be used 
      (IMPORTANT: should look like this, for instance: "synthetic_functions.branin")
    
directory: path in which results.csv should be stored
    
experiment_name: where all your results should be stored in mongo database (eg "test_robo")
    
logpath: path/to/mongoDB/logfile
    
dbpath: path/to/mongoDB/data/db
    
spearmint_path: path/to/Spearmint/spearmint


spearmint_wrapper.create_files(Task, dirpath, experiment_name, logpath, 
                               dbpath, spearmint_path )


