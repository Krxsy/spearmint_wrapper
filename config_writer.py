from collections import OrderedDict
import json

def config_writer(one, two, experiment_name):

    dic = OrderedDict([
        ("language", "PYTHON"),
        ("main-file", "RoBo_to_spearmint.py"),
        ("experiment-name", experiment_name),
        ("likelihood", "NOISELESS"),
        ("variables", OrderedDict([]))
        ])
    # update min and max values
    assert(len(one) == len(two))
    for i in range(len(one)):
        variable_name = 'x%i' %(i+1)
        dic['variables'][variable_name] = {}
        # set variable range to values defined in the given arrays
        dic['variables'][variable_name]['min'] = one[i]
        dic['variables'][variable_name]['max'] = two[i]
        dic['variables'][variable_name]['type'] = "FLOAT"
        dic['variables'][variable_name]['size'] = 1
            
    # print resulting variable ranges
    for key in OrderedDict.iterkeys(dic['variables']):
        min_value = dic['variables'][key]['min']
        max_value = dic['variables'][key]['max']
        print("%s = (%f, %f)" % (key, min_value, max_value))
    
    out_file = open("config.json","w")
    out_file.write(json.dumps(dic, indent=4))