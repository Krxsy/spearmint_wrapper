import subprocess
import os
import output2csv

def class_name_to_file_writer(class_name, experiment_name):
    """ 
    changes the class to be instantiated from robo.task 
    and also changes the experiment name accordingly.
    """
    file_name = os.path.dirname(os.path.realpath(__file__)) + "/RoBo_to_spearmint.py"
    with open(file_name) as f:
        lines = f.readlines()
    for i in range(len(lines)):
        line = lines[i]
        if line.startswith("TASK_CLASS_NAME = "):
            lines[i] = "TASK_CLASS_NAME = '" + class_name + "'\n"
        if line.startswith("config_writer(lower, upper,"):
            lines[i] = "config_writer(lower, upper, '" + experiment_name + "')\n"
    with open(file_name, "w") as f:
        for line in lines:
            f.write(line)

def create_files(task_class_name, directory, experiment_name,
                 logpath, dbpath, spearmint_path):
    """ 
    starts mongoDB daemon and spearmint 
    and finally writes the result into a csv file.
    
    task_class_name: the robo task to be used 
                    (IMPORTANT: first char has to be uppercase eg. "Branin")
    
    directory: path in which the results should be stored
    
    experiment_name: where all your results should be stored in mongo database
    
    logpath: path/to/mongoDB/logfile
    
    dbpath: path/to/mongoDB/dbfolder
    
    spearmint_path: path/to/Spearmint/spearmint
    """
    
    # first load chosen robo task
    class_name_to_file_writer(task_class_name, experiment_name)
        
    # 1st run file in order to generate config file 
    print('creating new config file')
    f_name = os.path.dirname(os.path.realpath(__file__)) + "/RoBo_to_spearmint.py"
    os.system("python %s" % (f_name))
    
    # start mongoDB daemon instance
    print('..starting  MongoDB daemon instance..')
    os.system("mongod --fork --logpath %s/mongodb.log --dbpath %s" % (logpath, dbpath))
    
    # start spearmint
    print('..starting spearmint..')
    experiment_dir = os.path.dirname(os.path.realpath(__file__))
    cmd = ["python %s/main.py %s" % (spearmint_path, experiment_dir)]
    process = subprocess.Popen(cmd, stdout=subprocess.PIPE, shell=True)
    output = process.communicate()[0]
    print "Output:", output
    
    process.wait()
    print('Done')
    
    # finally store results into your directory:
    output2csv.write_output(experiment_name, directory)
    # getting hyperparameter information
    output2csv.get_hypers(experiment_name, directory)
    
