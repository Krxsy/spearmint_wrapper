import csv
import numpy as np
import pymongo
from spearmint.utils.compression import decompress_nested_container

def write_output(experiment_name, dirname):
    '''
    Used for getting the output out of the mongoDB
    '''
    try:
        client = pymongo.MongoClient()
        print("Connected successfully!!!")
    except pymongo.errors.ConnectionFailure, e:
        print("Could not connect to MongoDB: %s" % e )  
    
    #db = client['spearmint']
    #print(client.database_names())
    db = client.spearmint
    #print(db.collection_names())
    dbcollection_jobs = db[experiment_name].jobs
    docs_jobs = list(dbcollection_jobs.find({}))
    decompressed_stuff = [decompress_nested_container(docs_jobs) for dbdoc in docs_jobs]
    #print(decompressed_stuff)
    start_times = []
    end_times = []
    params = []
    results = []
    job_ids = []
    dic_jobs = decompressed_stuff[0]
    for i in range(len(dic_jobs)):        
        for key, value in dic_jobs[i].items():
            if key == 'id':
                job_ids.append(value)
            if key == 'start time':
                start_times.append(value)
            if key == 'end time':
                end_times.append(value)
            if key == 'params':
                params.append(value)
            if key == 'values':
                results.append(value['main'])
                
    param_settings = []
    for j in range(len(params)):
        temp = {}
        for key, value in params[j].items():
            temp[key] = (value['values'][0])
        param_settings.append(temp)
        
    times = np.array(end_times) - np.array(start_times)        
    data = np.array([job_ids, times, results, param_settings]).T
    # getting overall time
    all_times = np.sum(times)
    print('Spearmint needed %f seconds' % (all_times))
    with open('%s/results.csv' %(dirname), 'w') as output:
            writer = csv.DictWriter(output, fieldnames = ["job_id", "time [secs]", "result", "params"], delimiter = ';')
            writer.writeheader()
            writer = csv.writer(output,lineterminator='\n')
            writer.writerows(data)
            
            
def get_hypers(experiment_name, dirname):
    '''
    Connects to the MongoDB in order to get additional information.
    Therefore spearmint stores two collections:

    hypers: some additional information about the hyperparameters: 
    amp2, ls, beta_beta, beta_alpha, mean
    
    Here: we only get the information on the hyperparameters
    '''
    # connection to Mongo DB
    try:
        client = pymongo.MongoClient()
        print("Connected successfully!!!")
    except pymongo.errors.ConnectionFailure, e:
        print("Could not connect to MongoDB: %s" % e )  
    
    #print(client.database_names())
    db = client.spearmint
    #print(db.collection_names())
    
    # there are two collections available: hypers and jobs
    dbcollection_hypers = db[experiment_name].hypers
    docs_hypers = dbcollection_hypers.find()
    docs_hypers = list(dbcollection_hypers.find({}))

    decompressed_stuff = [decompress_nested_container(docs_hypers) for dbdoc in docs_hypers]
    
    amp2 = decompressed_stuff[0][0]["main"]["hypers"]["amp2"]
    beta_alpha = decompressed_stuff[0][0]["main"]["hypers"]["beta_alpha"]
    ls = decompressed_stuff[0][0]["main"]["hypers"]["ls"]
    beta_beta = decompressed_stuff[0][0]["main"]["hypers"]["beta_beta"]
    mean = decompressed_stuff[0][0]["main"]["hypers"]["mean"]
    hypers = [amp2, beta_alpha, ls, beta_beta, mean]

    with open('%s/hypers.csv' %(dirname), 'w') as output:
        writer = csv.DictWriter(output, fieldnames = ["amp2", "beta_alpha", "ls", "beta_beta", "mean"], delimiter = ';')
        writer.writeheader()
        writer = csv.writer(output,lineterminator='\n')
        writer.writerow(hypers)    
