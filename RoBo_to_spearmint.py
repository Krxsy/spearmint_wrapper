import numpy as np
from config_writer import config_writer

TASK_CLASS_NAME = 'Branin'


def my_import(name):
    """imports and returns a module that is given as a string"""
    m = __import__(name)
    for n in name.split(".")[1:]:
        m = getattr(m, n)
    return m


module_name = "robo.task." + TASK_CLASS_NAME

imported_module = my_import(module_name)

# get constructor of the imported module and instanciate it
instance = TASK_CLASS_NAME.split('.')[1]

task_constructor = getattr(imported_module, instance.title())

task = task_constructor()
lower = task.X_lower
upper = task.X_upper

config_writer(lower, upper, 'test_robo')

def main(job_id, params):
    parameters = []
    for i in range(len(lower)):
        parameters.append(params['x%i'%(i+1)])
    print(parameters)
    result = task.objective_function(np.array(parameters).T)
    while isinstance(result, np.ndarray):
        result = result[0]
    return result
                                                                                                                                                                                                                                                
